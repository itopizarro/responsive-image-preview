'use strict';

var 
    browserSync = require( 'browser-sync' ),
    gulp = require( 'gulp' ),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require( 'gulp-sourcemaps' ),
    config = {
        'theme': {
            'path': ''
        },
        'source': {
            'path': 'source/',
            'sass': {
                'path': 'scss/',
                'files': '**/*.scss'
            }
        },
        'asset': {
            'path': 'dist/',
            'css': {
                'path': 'css',
                'filename': 'app.css' // just in case
            }
        }
    }
;

gulp.task('browsersync', function () {

    console.log('---------------------------------');
    console.log(' Watching path: ' + config.theme.path + config.asset.path + config.asset.css.path + '/**/*.css' );
    console.log('---------------------------------');

    browserSync({
        proxy: 'localhost:3001',
        files: config.theme.path + config.asset.path + config.asset.css.path + '/**/*.css',
        port: 8082
    });

});

/** Style Compilation **/
gulp.task( 'styles', function(){

    console.log('---------------------------------');
    console.log(' Processing SCSS: ' + config.theme.path + config.source.path + config.source.sass.path + config.source.sass.files );
    console.log('---------------------------------');
    
    return gulp.src( [ config.theme.path + config.source.path + config.source.sass.path + config.source.sass.files ] )
        .pipe( sourcemaps.init() )
        .pipe( sass().on( 'error', sass.logError ) )
        // .pipe( rename( config.asset.css.filename ) )
        .pipe( sourcemaps.write() )
        .pipe( gulp.dest( config.theme.path + config.asset.path + config.asset.css.path ) ) //uncompressed CSS
        ;
});

gulp.task( 'watch', function(){
    gulp.watch( config.theme.path + config.source.path + config.source.sass.path + config.source.sass.files, ['styles']);
});

gulp.task( 'default', [ 'styles', 'watch' ] );
gulp.task( 'build', [ 'styles' ] ); // no optimizer, yet