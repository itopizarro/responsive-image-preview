( function( window, document, undefined ){ 
    'use strict';
    var 
        init,
        onSelectFile,
        updateImage,
        $targets
    ;
    
    init = function (){
        // console.log( 'init', window.jQuery );
        $targets = $( '[ style*="background-image" ]' );
        document.getElementById( 'upload-file' ).addEventListener( 'change', onSelectFile, false );
    };

    onSelectFile = function ( event ){
        // console.log( 'onSelectFile', event.target.files, event.target.files[0].type );
        var file,
            reader
            ;
        event.preventDefault();
        file = event.target.files[0];
        if ( !file.type.match( 'image.*' ) ){
            return;
        }
        // console.log( 'match', file );
        reader = new FileReader();
        reader.onload = ( function ( _file ){
            // console.log( 'onload', arguments, _file );
            return function ( event ){
                // console.log( 'event??', event.target.result );
                updateImage( event.target.result );
            };
        } )( file );

        reader.readAsDataURL( file );
    };

    updateImage = function ( reference ){
        // console.log( 'updateImage', reference );
        $targets.each( function ( index, element ){
            $( element ).css( 'background-image', 'url(' + reference + ')' );
        } );
    };

    window.onload = init;

} )( this, document );