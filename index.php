<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Responsive Image Preview Page</title>
    <link rel="stylesheet" href="dist/css/styles.css" type="text/css" />
</head>
<body>

    <div class="page-container">

        <section class="full-screen">
            <div class="inner">
                <figure style="background-image: url( '//placekitten.com/999' );"></figure>
                <h1>Responsive Image Preview Page</h1>
            </div>
        </section>

        <section class="bleed-right">
            <div class="inner">
                <div class="copy">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, molestias.</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, quisquam, eveniet, iste recusandae ratione velit nisi aliquam ex natus voluptas omnis animi non sit quo sunt porro nulla quidem rerum quasi dolores doloribus iure rem reprehenderit nemo quis iusto laboriosam quos nam dolorum qui!</p>
                    </div>
                <figure style="background-image: url( '//placekitten.com/999' );"></figure>
            </div>
        </section>

        <section class="bleed-left">
            <div class="inner">
                <div class="copy">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, molestias.</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, quisquam, eveniet, iste recusandae ratione velit nisi aliquam ex natus voluptas omnis animi non sit quo sunt porro nulla quidem rerum quasi dolores doloribus iure rem reprehenderit nemo quis iusto laboriosam quos nam dolorum qui!</p>
                    </div>
                <figure style="background-image: url( '//placekitten.com/999' );"></figure>
            </div>
        </section>

        <section class="bleed-right gradient-transition">
            <div class="inner">
                <div class="copy">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, molestias.</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, quisquam, eveniet, iste recusandae ratione velit nisi aliquam ex natus voluptas omnis animi non sit quo sunt porro nulla quidem rerum quasi dolores doloribus iure rem reprehenderit nemo quis iusto laboriosam quos nam dolorum qui!</p>
                    </div>
                <figure style="background-image: url( '//placekitten.com/999' );"></figure>
            </div>
        </section>

        <section class="bleed-left gradient-transition">
            <div class="inner">
                <div class="copy">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, molestias.</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, quisquam, eveniet, iste recusandae ratione velit nisi aliquam ex natus voluptas omnis animi non sit quo sunt porro nulla quidem rerum quasi dolores doloribus iure rem reprehenderit nemo quis iusto laboriosam quos nam dolorum qui!</p>
                    </div>
                <figure style="background-image: url( '//placekitten.com/999' );"></figure>
            </div>
        </section>

        <section class="bleed-both">
            <div class="inner">
                <div class="copy">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, molestias.</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, architecto, odit iusto consectetur id aspernatur sequi dolorem officia nemo nesciunt.</p>
                </div>
                <figure style="background-image: url( '//placekitten.com/999' );"></figure>
            </div>
        </section>
        
        <aside>
            <input type="file" id="upload-file" name="files" multiple />
            <label for="upload-file">Upload a file to test</label>
            <div class="message success">Currently viewing: <code class="file-name"></code></div>
            <div class="message error">Uh-oh! <span class="error-message"></span></div>
        </aside>

    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="source/scripts/main.js"></script>

    <script id="__bs_script__">
        document.write("<script async src='https://HOST:8082/browser-sync/browser-sync-client.js?v=2.18.8'><\/script>".replace("HOST", location.hostname));
    </script>

</body>
</html>