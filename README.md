    ▄▄▄  ▄▄▄ ..▄▄ ·  ▄▄▄·       ▐ ▄ .▄▄ · ▪   ▌ ▐·▄▄▄ .    
    ▀▄ █·▀▄.▀·▐█ ▀. ▐█ ▄█▪     •█▌▐█▐█ ▀. ██ ▪█·█▌▀▄.▀·    
    ▐▀▀▄ ▐▀▀▪▄▄▀▀▀█▄ ██▀· ▄█▀▄ ▐█▐▐▌▄▀▀▀█▄▐█·▐█▐█•▐▀▀▪▄    
    ▐█•█▌▐█▄▄▌▐█▄▪▐█▐█▪·•▐█▌.▐▌██▐█▌▐█▄▪▐█▐█▌ ███ ▐█▄▄▌    
    .▀  ▀ ▀▀▀  ▀▀▀▀ .▀    ▀█▄▀▪▀▀ █▪ ▀▀▀▀ ▀▀▀. ▀   ▀▀▀     
    ▪  • ▌ ▄ ·.  ▄▄▄·  ▄▄ • ▄▄▄ .                          
    ██ ·██ ▐███▪▐█ ▀█ ▐█ ▀ ▪▀▄.▀·                          
    ▐█·▐█ ▌▐▌▐█·▄█▀▀█ ▄█ ▀█▄▐▀▀▪▄                          
    ▐█▌██ ██▌▐█▌▐█ ▪▐▌▐█▄▪▐█▐█▄▄▌                          
    ▀▀▀▀▀  █▪▀▀▀ ▀  ▀ ·▀▀▀▀  ▀▀▀                           
     ▄▄▄·▄▄▄  ▄▄▄ . ▌ ▐·▪  ▄▄▄ .▄▄▌ ▐ ▄▌                   
    ▐█ ▄█▀▄ █·▀▄.▀·▪█·█▌██ ▀▄.▀·██· █▌▐█                   
     ██▀·▐▀▀▄ ▐▀▀▪▄▐█▐█•▐█·▐▀▀▪▄██▪▐█▐▐▌                   
    ▐█▪·•▐█•█▌▐█▄▄▌ ███ ▐█▌▐█▄▄▌▐█▌██▐█▌                   
    .▀   .▀  ▀ ▀▀▀ . ▀  ▀▀▀ ▀▀▀  ▀▀▀▀ ▀▪                   

## Responsive Image What?

It's a page. To test your ridiculous and unreasonable responsive image expectations against cold, hard reality.

## TODO
* Create input/file upload and upload/test button elements
* Style file upload
* add upload success/error states
* add current filename message
* add cancel user-image (revert to placekitten?)
* add option to use image from URL
* Add image-positioning options (radio/checkboxes?)
* Add "code output" area -- and supporting functionality to spit out the CSS(& HTML?) necessary to replicate effect/positioning